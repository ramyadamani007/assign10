#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define BUFFER_SIZE 1000

void remove_all (char* str, const char* rem);

int main()
{
	FILE *fp, *fr;
	char buffer [1000], rem[100];
	
	printf("Enter a word you want to remove.\n");
	scanf("%s",rem);
	
	fp = fopen ("f3.txt","r");
	fr = fopen ("delete.tmp","w");
	
	if (fr == NULL && fp == NULL)
	{
		printf("File cannot open.\n");
		exit (1);
	}
	
	while (fgets(buffer, 1000, fp)!= NULL)
	{
		remove_all (buffer, rem);
		fputs (buffer,fr);
	}
	fclose(fp);
	fclose(fr);
	
	remove("f3.txt");
	rename("delete.txt","f3.txt");
	printf("\nAll occurrence of '%s' removed successfully.\n", rem);
	return 0;
}



void remove_all(char * str, const char * rem)
{
    int i, j, stringLen, toRemoveLen;
    int found;

    stringLen   = strlen(str);      // Length of string
    toRemoveLen = strlen(rem); // Length of word to remove


    for(i=0; i <= stringLen - toRemoveLen; i++)
    {
        /* Match word with string */
        found = 1;
        for(j=0; j < toRemoveLen; j++)
        {
            if(str[i + j] != rem[j])
            {
                found = 0;
                break;
            }
        }

        /* If it is not a word */
        if(str[i + j] != ' ' && str[i + j] != '\t' && str[i + j] != '\n' && str[i + j] != '\0') 
        {
            found = 0;
        }

        /*
         * If word is found then shift all characters to left
         * and decrement the string length
         */
        if(found == 1)
        {
            for(j=i; j <= stringLen - toRemoveLen; j++)
            {
                str[j] = str[j + toRemoveLen];
            }

            stringLen = stringLen - toRemoveLen;

            // We will match next occurrence of word from current index.
            i--;
        }
    }
}









		
