/*******************
	Include
*******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/****** MAIN ******/

int main()
{
	FILE *fp,*fr;
	char c;
	fp = fopen ("f1.txt","r");
	fr = fopen ("f2.txt","w");
	if (fp == NULL)
	{
		printf("File not found.\n");
		exit (1);
	}
	
	while (!feof(fp))
	{
		c = fgetc(fp);
		fputc (c,fr);
	}
	fclose(fr);
	fclose(fp);
	
}
