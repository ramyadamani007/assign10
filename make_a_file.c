/*******************
	Include
*******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/****** MAIN ******/

int main()
{
	int i;
	FILE *fp;
	char s[100];
	fp = fopen ("f1.txt","w");
	if (fp == NULL)
		{
		printf("File is not open.\n");
		exit (1);
		}
	printf("Enter a String.\n");
	gets (s);
	for (i=0; i<strlen (s); i++)
		{
		fputc(s[i],fp);
		}
	fclose(fp);
	return 0;
}
		
