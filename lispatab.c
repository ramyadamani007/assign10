/*******************
	Include
*******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/****** MAIN ******/

int main()
{
	char ch;
	FILE *fp;
	int i=0,l=0,sp=0,tab=0;
	
	
	fp = fopen ("f1.txt","r");
	if (fp == NULL)
	{
		printf("File does not exist.\n");
		exit (1);
	}
	
	while (!feof(fp))
	{
		ch = fgetc(fp);
		i ++;
		if (ch == ' ')
			sp++;
		if (ch == '\n')
			l++;
		if (ch == '	')
			tab++;

	}
	printf("Number of Characters = %d\n", i);
	printf("Number of Spaces = %d\n", sp);
	printf("Number of lines = %d\n", l);
	printf("Number of Tabs = %d\n", tab);
	fclose (fp);
	return 0;
}






