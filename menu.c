/*******************
	Include
*******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/****** MAIN ******/

int main()
{
	FILE *fp,*fr;
	char ch,a[100],b[100];
	int choice;
	printf("Select a choice from below given choices.\n");
	printf("MENU \n");
	printf("1. Set Buffer.\n");
	printf("2. Print Buffer.\n");
	printf("3. exit.\n");
	scanf("%d",&choice);
	
	switch(choice)
		{
		case 1:
			printf("Set Buffer.\n");
			fp = fopen("f3.txt","w");
			scanf("%s",a);
			fputs(a,fp);
			fclose(fp);
			exit(1);
			
		case 2:
			printf("Print Buffer.\n");
			fr = fopen ("f2.txt","r");
			fgets (b, 100, fr);
			puts(b);
			fclose(fr);
			exit (1);
			
		case 3: 
			printf("exit.\n");
			exit (1);
			
		default:
			printf("Invalid Choice.\n");
			
		}
	return 0;
}
			
			
			
